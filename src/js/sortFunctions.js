const sortFunctions = {
  methods: {
    sortById(candidates) {
      return candidates.sort((a, b) => {
        if (a !== undefined && a.Likes !== undefined && b !== undefined && b.Likes !== undefined) {
          if (a.Id > b.Id) {
            return 1;
          } if (a.Id < b.Id) {
            return -1;
          }
          return 0;
        }
        return 0;
      });
    },
    sortByLikesAsc(candidates) {
      return candidates.sort((a, b) => {
        if (a !== undefined && a.Likes !== undefined && b !== undefined && b.Likes !== undefined) {
          if (a.Likes > b.Likes) {
            return 1;
          } if (a.Likes < b.Likes) {
            return -1;
          }
          return 0;
        }
        return 0;
      });
    },
    sortByLikesDesc(candidates) {
      return candidates.sort((a, b) => {
        if (a !== undefined && a.Likes !== undefined && b !== undefined && b.Likes !== undefined) {
          if (a.Likes > b.Likes) {
            return -1;
          } if (a.Likes < b.Likes) {
            return 1;
          }
          return 0;
        }
        return 0;
      });
    },
    sortByAgeAsc(candidates) {
      return candidates.sort((a, b) => {
        if (a !== undefined && a.Geburtstag !== undefined
          && b !== undefined && b.Geburtstag !== undefined) {
          const aDateArr = a.Geburtstag.split('.');
          const bDateArr = b.Geburtstag.split('.');
          const dateA = Date.parse(aDateArr[2], aDateArr[1], aDateArr[0]);
          const dateB = Date.parse(bDateArr[2], bDateArr[1], bDateArr[0]);
          if (dateA > dateB) {
            return -1;
          } if (dateA < dateB) {
            return 1;
          }
          return 0;
        }
        return 0;
      });
    },
    sortByAgeDesc(candidates) {
      return candidates.sort((a, b) => {
        if (a !== undefined && a.Geburtstag !== undefined
          && b !== undefined && b.Geburtstag !== undefined) {
          const aDateArr = a.Geburtstag.split('.');
          const bDateArr = b.Geburtstag.split('.');
          const dateA = Date.parse(aDateArr[2], aDateArr[1], aDateArr[0]);
          const dateB = Date.parse(bDateArr[2], bDateArr[1], bDateArr[0]);
          if (dateA > dateB) {
            return 1;
          } if (dateA < dateB) {
            return -1;
          }
          return 0;
        }
        return 0;
      });
    },
  },
};
export default sortFunctions;
